public class Out {
    public void say () {
        System.out.println("this is out class");
    }

    public static class In {
        public void say2 () {
            System.out.println("this is inner class");
        }

        public static class Inin {
            public void say3 () {
                System.out.println("this is an inner class inside of an inner class");
                class MethodClass {
                    public void prnt () {
                        System.out.println("this is a class inside of a method");
                    }
                }
                MethodClass mtd = new MethodClass();
                mtd.prnt();
            }
        }
    }
}
