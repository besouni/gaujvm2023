import java.io.FileWriter;
import java.io.IOException;

public class FamilyBudget {
    private int money = 0;
    private String updateMoney = "";

    public int getMoney() {
        return money;
    }

    public void setMoney(int money) {
        this.money = money;
        updateMoney += money+"\n";
    }

    public void changeMoney(int amount){
        this.money += amount;
        updateMoney += money+"\n";
    }

    public void checkWealth(){
        if(money>40000){
            System.out.println("Rich!");
        }else if(money>=10000){
            System.out.println("Average!");
        }else{
            System.out.println("poor");
        }
    }

    public void saveMoneyInFile(){
        try {
            FileWriter fileWriter = new FileWriter("budget.txt");
            fileWriter.write(this.updateMoney);
            fileWriter.close();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
