import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Random;

// Press Shift twice to open the Search Everywhere dialog and type `show whitespaces`,
// then press Enter. You can now see whitespace characters in your code.
public class Main {
    public static void main(String[] args) {
        if(Files.exists(Path.of("family.txt"))){
            try {
                Files.delete(Path.of("family.txt"));
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
        Random r = new Random();
        FamilyBudget familyBudget = new FamilyBudget();
        familyBudget.setMoney(10000);
        System.out.println("Family Budget equal "+familyBudget.getMoney());
        familyBudget.changeMoney(r.nextInt(-3000, 3000));
        System.out.println("Family Budget equal "+familyBudget.getMoney());
        familyBudget.changeMoney(r.nextInt(-3000, 3000));
        System.out.println("Family Budget equal "+familyBudget.getMoney());
        familyBudget.changeMoney(r.nextInt(-3000, 3000));
        System.out.println("Family Budget equal "+familyBudget.getMoney());
        familyBudget.changeMoney(-9000);
        System.out.println("Family Budget equal "+familyBudget.getMoney());
        familyBudget.saveMoneyInFile();
//        familyBudget.checkWealth();
        FamilyMember familyMember1 = new FamilyMember("Niko", "Buadze", "Father", 50);
        FamilyMember familyMember2 = new FamilyMember("Nino", "Gedenidze", "Mother", 45);
    }
}