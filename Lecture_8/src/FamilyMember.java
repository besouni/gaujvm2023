import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

public class FamilyMember {
    String name, lastName, status;
    int age;

    public FamilyMember(String name, String lastName, String status, int age) {
        this.name = name;
        this.lastName = lastName;
        this.status = status;
        this.age = age;
        saveMemberInfoToFile();
    }

    public void saveMemberInfoToFile(){
        try {
            FileWriter fileWriter = new FileWriter("family.txt", true);
            fileWriter.write(this.name+", "+this.lastName+", "+this.status+", "+this.age+"\n");
            fileWriter.close();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
