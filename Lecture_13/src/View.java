import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class View {

    @FXML
    public TextField name;

    @FXML
    public TextField lastName;

    @FXML
    public TextField email;

    @FXML
    public PasswordField password;

    @FXML
    public Label ms;

    public void insert(){
        System.out.println(name.getText());
        System.out.println(lastName.getText());
        System.out.println(email.getText());
        System.out.println(password.getText());
        String url = "jdbc:mysql://localhost:3306/test?useUnicode=true&characterEncoding=utf-8";
        try {
            Connection connection = DriverManager.getConnection(url, "root", "");
            PreparedStatement statement = connection.prepareStatement("INSERT INTO users(name, lastname, email, password) VALUES (?, ?, ?, ?)");
            statement.setString(1, name.getText());
            statement.setString(2, lastName.getText());
            statement.setString(3, email.getText());
            statement.setString(4, password.getText());
            statement.executeUpdate();
            name.clear();
            lastName.clear();
            email.clear();
            password.clear();
            ms.setText("New record created!!!");
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}
