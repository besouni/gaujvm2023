import java.sql.*;

// Press Shift twice to open the Search Everywhere dialog and type `show whitespaces`,
// then press Enter. You can now see whitespace characters in your code.
public class Main {
    public static void main(String[] args) {
      String url = "jdbc:mysql://localhost:3306/test?useUnicode=true&characterEncoding=utf-8";
        try {
            Connection con = DriverManager.getConnection(url, "root", "");
            Statement statement = con.createStatement();
            ResultSet res =  statement.executeQuery("SELECT * FROM users");
            res.next();
            System.out.println(res.getInt("id"));
            System.out.println(res.getString("password"));
            res.next();
            System.out.println(res.getInt("id"));
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

    }
}