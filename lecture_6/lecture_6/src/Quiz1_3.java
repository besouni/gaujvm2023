import java.util.Random;
import java.util.Scanner;

public class Quiz1_3 {

    int a=0, b=0;
    Scanner scanner = new Scanner(System.in);
    public Quiz1_3(){
        System.out.print("a=");
        a=scanner.nextInt();
        System.out.print("b=");
        b=scanner.nextInt();
        if(a>b){
            int c = a;
            a = b;
            b = c;
        }
        generateRandomNumbers(30);
    }
    private void generateRandomNumbers(int N){
        Random r = new Random();
        int rand = 0, c=0;
        for(int i=0; i<N; i++){
            rand = r.nextInt(a, b);
            System.out.println(rand+"  "+checkNumber(rand));
            if(checkNumber(rand)){
                c++;
            }
        }
        System.out.println("C = "+c);
    }

    private boolean checkNumber(int N){
        for(int i=2; i<=Math.sqrt(N); i++){
            if( N%i==0 ){
                return false;
            }
        }
        return true;
    }
}
