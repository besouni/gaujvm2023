public class Person {
    public static String className = "Person";
    public String name = "User";
    public int age;
    public String ID;

    public Person(){
        System.out.println("This is a constructor of Person!!");
    }

    public Person(String name, int asaki){
        this.name =  name;
        age = asaki;
    }

    public void printName(){
        System.out.println("Name: "+name);
    }

    public void printAge(){
        System.out.println("Age: "+this.age);
    }
}
