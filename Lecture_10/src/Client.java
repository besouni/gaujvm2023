import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.InetAddress;
import java.net.Socket;
import java.util.Scanner;

public class Client extends Thread {
    Integer N = 20;
    Socket socket;
    ObjectOutputStream out;
    String message;


    public void run(){
        try {
            socket = new Socket(InetAddress.getByName("192.168.122.19"), 8001);
            out = new ObjectOutputStream(socket.getOutputStream());
            Scanner scanner = new Scanner(System.in);
            System.out.print("Client message: ");
            message = scanner.nextLine();
            out.writeObject(message);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

//        test();
    }

    public void test(){
        for(int i=0; i<N; i++) {
            System.out.println("Cleint -> "+i);
        }
    }
}
