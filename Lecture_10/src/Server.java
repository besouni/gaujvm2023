import java.io.IOException;
import java.io.ObjectInputStream;
import java.net.ServerSocket;
import java.net.Socket;

public class Server implements Runnable{
    Integer N = 20;
    Socket socket;
    ObjectInputStream in;
    String result;

    @Override
    public void run() {
        try {
            ServerSocket serverSocket = new ServerSocket(8001);
            socket = serverSocket.accept();
            in = new ObjectInputStream(socket.getInputStream());
            result = (String) in.readObject();
            System.out.println("Server:"+result);

        } catch (IOException e) {
            throw new RuntimeException(e);
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
//        test();
    }

    public void test(){
        for(int i=0; i<N; i++) {
            System.out.println("Server -> "+i);
        }
    }
}
